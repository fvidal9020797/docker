FROM centos:7

RUN yum -y update
RUN yum -y install java-1.8.0-openjdk-devel
RUN curl https://downloads.lightbend.com/scala/2.12.10/scala-2.12.10.rpm --output scala-2.12.10.rpm
RUN yum -y install scala-2.12.10.rpm
RUN curl https://bintray.com/sbt/rpm/rpm | tee /etc/yum.repos.d/bintray-sbt-rpm.repo
RUN yum -y install sbt
RUN yum install -y git
RUN yum install -y which
RUN yum install -y gcc-c++ make
RUN curl -sL https://rpm.nodesource.com/setup | bash -
RUN yum install -y nodejs
RUN mkdir /work


WORKDIR /work
COPY . .
RUN npm install
EXPOSE 3000
CMD ["node", "app.js"]

